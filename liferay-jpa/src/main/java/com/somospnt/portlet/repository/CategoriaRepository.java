package com.somospnt.portlet.repository;

import com.somospnt.portlet.domain.Categoria;
import org.springframework.data.repository.CrudRepository;

public interface CategoriaRepository extends CrudRepository<Categoria, Long>{


}
