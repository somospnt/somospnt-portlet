package com.somospnt.portlet.controller;

import com.somospnt.portlet.repository.CategoriaRepository;
import javax.portlet.RenderRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

@Controller
@RequestMapping("VIEW")
public class CategoriaController {

    @Autowired
    private CategoriaRepository repository;

    @RenderMapping
    public ModelAndView handleRenderRequest(RenderRequest request) {
        return new ModelAndView("view");
    }

    @ResourceMapping(value = "categorias")
    public ModelAndView categorias() {
        ModelAndView mav = new ModelAndView();
        mav.addObject("categorias", repository.findAll());
        mav.setView(new MappingJackson2JsonView());
        return mav;
    }

}
