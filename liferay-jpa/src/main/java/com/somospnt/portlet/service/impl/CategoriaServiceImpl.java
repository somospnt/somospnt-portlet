package com.somospnt.portlet.service.impl;

import com.somospnt.portlet.domain.Categoria;
import com.somospnt.portlet.repository.CategoriaRepository;
import com.somospnt.portlet.service.CategoriaService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CategoriaServiceImpl implements CategoriaService {

    @Autowired
    private CategoriaRepository categoriaRepository;

    @Override
    public List<Categoria> buscarTodas() {
        return (List<Categoria>) categoriaRepository.findAll();
    }

}
