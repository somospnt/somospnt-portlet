package com.somospnt.portlet.service;

import com.somospnt.portlet.domain.Categoria;
import java.util.List;

public interface CategoriaService {

    List<Categoria> buscarTodas();

}
