<%--
/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
--%>

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>

<portlet:defineObjects />
<portlet:resourceURL id="categorias" var="categorias"></portlet:resourceURL>

<img style="-webkit-user-select: none; cursor: zoom-in;" src="http://image.slidesharecdn.com/springconfiguration-140525093731-phpapp02/95/spring-configuration-so-long-spring-xmls-1-638.jpg?cb=1401010813" width="527" height="396">

<script type="text/javascript">
    $(document).ready(function(){

        $.ajax({
            url: "${categorias}",
            type: 'GET',
            datatype: 'json',
            success: function (data) {
                console.log(data);
            }
        });

    });

</script>